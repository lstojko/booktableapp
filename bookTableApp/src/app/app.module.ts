import { BrowserModule } from '@angular/platform-browser';
import { NgModule,  NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { TableListComponent } from './Containers/table-list/table-list.component';
import { LayoutComponent } from './LayoutComponents/layout/layout.component';
import { RestaurantComponent } from './LayoutComponents/restaurant/restaurant.component';
import { NavigationComponent } from './LayoutComponents/navigation/navigation.component';
import { FooterComponent } from './LayoutComponents/footer/footer.component';
import { PickTableComponent } from './Containers/pick-table/pick-table.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RestaurantPickComponent } from './LayoutComponents/restaurant-pick/restaurant-pick.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { SvgComponentComponent } from './LayoutComponents/restaurant/svg-component/svg-component.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    TableListComponent,
    LayoutComponent,
    RestaurantComponent,
    NavigationComponent,
    FooterComponent,
    PickTableComponent,
    RestaurantPickComponent,
    SvgComponentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    AngularFontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
  