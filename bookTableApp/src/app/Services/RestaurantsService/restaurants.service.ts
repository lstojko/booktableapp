import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  private restaurants = [{
    title: 'Mr Pancake',
    subtitle: "Włącz się do zabawy",
    city: 'Poznan',
    street: 'Batorego',
    number: '8',
    style: "'Srisakdi', cursive"
  },{
    title: 'Noz i Widelec',
    subtitle: "Sztućce to narzędzia, by poznawać smaki",
    city: 'Poznan',
    street: 'Sobieskiego',
    number: '66',
    style: "'Lora', serif"
  },{
    title: 'U babci przy stole',
    subtitle: "...bo skąd wyjdziemy tak najedzeni?",
    city: 'Poznan',
    street: 'Polwiejska',
    number: '10',
    style: "'Open Sans Condensed', sans-serif"
  }]

  constructor() {

   }

   getRestaurants(){
    return this.restaurants;
   }
}
