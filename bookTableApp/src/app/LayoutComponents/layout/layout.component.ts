import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  private openModel: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  openRestaurantModel(event){
    console.log("Layout data come", event);
    this.openModel = event;
  }

}
