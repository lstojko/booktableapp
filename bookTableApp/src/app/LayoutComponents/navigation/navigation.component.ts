import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  private restaurantChange: string = '';
  private menuState: boolean = false;
  private searchBarValue: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  searchBar(event){
    console.log("ZMIANA RESTAURACJI ", event);
  }

  openMenu(){
    this.menuState = !this.menuState;
  }

  openSearchBar(){
    this.searchBarValue = !this.searchBarValue;
  }

}
