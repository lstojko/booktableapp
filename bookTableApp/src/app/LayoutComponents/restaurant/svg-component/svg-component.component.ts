import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import Snap, { mina } from 'snapsvg-cjs';

@Component({
  selector: 'app-svg-component',
  templateUrl: './svg-component.component.html',
  styleUrls: ['./svg-component.component.scss']
})
export class SvgComponentComponent implements OnInit {
  @ViewChild('snap') svg: ElementRef;
  private svgHeight: number = 0;
  private svgWidth: number = 0;
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    console.log("Native ", this.svg);
    this.svgHeight = this.svg.nativeElement.clientHeight;
    this.svgWidth = this.svg.nativeElement.clientWidth;
    console.log("Wysokość obrazu ", this.svgHeight);
    this.createRestaurantModel(this.svgHeight, this.svgWidth);
  }

  createRestaurantModel(height, width) {
    var s = Snap("#snappy");
    // Draw a rounded square.
    // Paper.rect(x, y, width, height, [rx], [ry])
    var restaurant = s.rect(5, 5, width-26, height-26, 0, 0);

     var doorBar = s.rect(width/2 - 50, 0, 100, 10, 0, 0);
    var window1 = s.rect(width-300 - 3*(width-300)/4, 0, 100, 10, 0, 0);
    var window2 = s.rect(width-100 - (width-300)/4, 0, 100, 10, 0, 0);
    var window3 = s.rect(0, (height-200)/3, 10, 100, 0, 0);
    var window4 = s.rect(width-25,  (height-200)/3, 10, 100, 0, 0);
    var window5 = s.rect(0, height-100-(height-200)/3, 10, 100, 0, 0);
    var window6 = s.rect(width-25,  height-100-(height-200)/3, 10, 100, 0, 0);
    var table1 = s.rect(width/3 - width/6 - 35, height/3 - height/6 - 35, 70, 70, 20, 20);
    var table2 = s.rect(2*width/3 - width/6 - 35,  height/3 - height/6 - 35, 70, 70, 20, 20);
    var table3 = s.rect(width/3 - width/6 - 35,  2*height/3 - height/6 - 35, 70, 70, 20, 20);
    var table4 = s.rect(2*width/3 - width/6 - 35, 2*height/3 - height/6 - 35, 70, 70, 20, 20);
    var table5 = s.rect(width/3 - width/6 - 35,  3*height/3 - height/6 - 35, 70, 70, 20, 20);
    var table6 = s.rect(2*width/3 - width/6 - 35, 3*height/3 - height/6 - 35, 70, 70, 20, 20);
    var table7 = s.rect(3*width/3 - width/6 - 35, height/3 - height/6 - 35, 70, 70, 20, 20);
    var table8 = s.rect(3*width/3 - width/6 - 35,  2*height/3 - height/6 - 35, 70, 70, 20, 20);
    var table9 = s.rect(3*width/3 - width/6 - 35, 3*height/3 - height/6 - 35, 70, 70, 20, 20);

    window1.attr({
      fill: "#568ce2"
    });
    window2.attr({
      fill: "#568ce2"
    });
    window3.attr({
      fill: "#568ce2"
    });
    window4.attr({
      fill: "#568ce2"
    });
    window5.attr({
      fill: "#568ce2"
    });
    window6.attr({
      fill: "#568ce2"
    });
    doorBar.attr({
      fill: "grey"
    });
    table1.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table2.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table3.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table4.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table5.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table6.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table7.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table8.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    table9.attr({
      fill: "white",
      stroke: "rgba(0,0,0,0.8)",
      strokeWidth: 3
    });
    restaurant.attr({
      fill: "rgba(255,255,255,0.5)",
      stroke: "white",
      strokeWidth: 3
    });
    let tables = [table1,table2, table3, table4, table5,table6, table7, table8, table9];

    tables.forEach( element => {
      element.hover(() => {
        element.animate({
          fill: 'rgba(244,209,173,1)'
        }, 1000)
      }
    )
    element.mouseout(() => {
      element.animate({
        fill: 'white'
      }, 1000)
    }
  )
    })

    // restaurant.mouseover(() => {
    //   tables.forEach( element => {
    //     element.mouseup(() => {
    //       element.animate({
    //         fill: 'white'
    //       }, 1000)
    //     }
    //   )
    //   })
    // })
      
    


  }

}
