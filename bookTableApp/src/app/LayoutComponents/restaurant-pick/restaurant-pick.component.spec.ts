import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantPickComponent } from './restaurant-pick.component';

describe('RestaurantPickComponent', () => {
  let component: RestaurantPickComponent;
  let fixture: ComponentFixture<RestaurantPickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
